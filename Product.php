<?php

class Product
{

    /**
     * @var ProductElement[]
     */
    private $elements = [];

    /**
     * @param ProductElement $productElement
     */
    public function addElement(ProductElement $productElement)
    {
        $this->elements[] = $productElement;
    }

}